package tictactoe;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        String[][] gameBoard = fillBoard();
        printBoard(gameBoard);
        game(gameBoard);
    }

    private static String[][] fillBoard() {
        int number = 0;
        String[][] board = new String[3][3];
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                number = number + 1;
                board[r][c] = Integer.toString(number);
            }
        }
        return board;
    }

    private static void upAndDownBorder() {
        System.out.println("/---|---|---\\");
    }

    private static void rowDivider() {
        System.out.println("\n|-----------|");
    }

    private static void printBoard(String[][] inputBoard) {
        upAndDownBorder();
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                System.out.print("|");
                System.out.print(" " + inputBoard[r][c] + " ");
            }
            System.out.print("|");
            if (r == 0 || r == 1) {
                rowDivider();
            }
        }
        System.out.println();
        upAndDownBorder();
    }

    private static void game(String[][] board) {
        System.out.println("Welcome to 2 player Tic Tac Toe");
        System.out.println("X will play first. Enter a slot number to place X in");
        int input = new Scanner(System.in).nextInt();
        int c;
        int r;
        c = column(input);
        r = row(input);
        board[r][c] = "X";
        r = 0;
        c = 0;
        printBoard(board);
        int numberOfRuns;
        for (numberOfRuns = 0; numberOfRuns < 4; numberOfRuns++) {
            boolean correctInput = false;
            boolean correctInput2 = false;
            while (!correctInput || !correctInput2) {
                System.out.println("O's turn; enter a slot number to place O in: ");
                Scanner inp = new Scanner(System.in);
                input = inp.nextInt();
                c = column(input);
                r = row(input);
                if (input > 9 || input < 1) {
                    System.out.println("invalid slot number, try again");
                    correctInput2 = false;
                } else correctInput2 = true;
                if ((board[r][c].equals("X") || board[r][c].equals("O")) && correctInput2) {
                    System.out.println("this slot is already taken, please choose another one");
                    correctInput = false;
                } else correctInput = true;

            }
            board[r][c] = "O";
            printBoard(board);
            if (checkXWinner(board) || checkOWinner(board)) {
                break;
            }
            correctInput = false;
            correctInput2 = false;
            while (!correctInput || !correctInput2) {
                System.out.println("X's turn; enter a slot number to place X in: ");
                Scanner inp = new Scanner(System.in);
                input = inp.nextInt();
                c = column(input);
                r = row(input);
                if (input > 9 || input < 1) {
                    System.out.println("invalid slot number, try again");
                    correctInput2 = false;
                } else correctInput2 = true;
                if ((board[r][c].equals("X") || board[r][c].equals("O")) && correctInput2) {
                    System.out.println("this slot is already taken, please choose another one");
                    correctInput = false;
                } else correctInput = true;
            }
            board[r][c] = "X";
            printBoard(board);
            if (checkXWinner(board) || checkOWinner(board)) {
                break;
            }
        }
        if (checkOWinner(board)) {
            System.out.println("The winner is O! congrats!");
        }
        if (checkXWinner(board)) {
            System.out.println("The winner is X! congrats!");
        }
        if (!checkXWinner(board) && !checkOWinner(board)) {
            System.out.println("Game over it is draw. there is no winner");
        }
    }

    private static boolean checkXWinner(String[][] board) {
        boolean isXWinner = false;
        int x;
        //check columns
        for (int c = 0; c < 3; c++) {
            x = 0;
            for (int r = 0; r < 3; r++) {
                if (board[r][c].equals("X")) {
                    x = x + 1;
                }
            }
            if (x == 3) isXWinner = true;
        }
        //check rows
        for (int r = 0; r < 3; r++) {
            x = 0;
            for (int c = 0; c < 3; c++) {
                if (board[r][c].equals("X")) {
                    x = x + 1;
                }
            }
            if (x == 3) isXWinner = true;
        }
        // checking diagonal 1
        x = 0;
        for (int r = 0, c = 0; r < 3 && c < 3; r++, c++) {
            if (board[r][c].equals("X")) {
                x = x + 1;
            }
        }
        if (x == 3) isXWinner = true;
        //checking diagonal 2
        x = 0;
        for (int r = 0, c = 2; r < 3 && c >= 0; r++, c--) {
            if (board[r][c].equals("X")) {
                x = x + 1;
            }
        }
        if (x == 3) isXWinner = true;
        return isXWinner;
    }

    private static boolean checkOWinner(String[][] board) {
        boolean isOWinner = false;
        int x;
        //check columns
        for (int c = 0; c < 3; c++) {
            x = 0;
            for (int r = 0; r < 3; r++) {
                if (board[r][c].equals("O")) {
                    x = x + 1;
                }
            }
            if (x == 3) isOWinner = true;
        }
        //check rows
        for (int r = 0; r < 3; r++) {
            x = 0;
            for (int c = 0; c < 3; c++) {
                if (board[r][c].equals("O")) {
                    x = x + 1;
                }
            }
            if (x == 3) isOWinner = true;
        }
        // checking diagonal 1
        x = 0;
        for (int r = 0, c = 0; r < 3 && c < 3; r++, c++) {
            if (board[r][c].equals("O")) {
                x = x + 1;
            }
        }
        if (x == 3) isOWinner = true;
        //checking diagonal 2
        x = 0;
        for (int r = 0, c = 2; r < 3 && c >= 0; r++, c--) {
            if (board[r][c].equals("O")) {
                x = x + 1;
            }
        }
        if (x == 3) isOWinner = true;
        return isOWinner;
    }

    private static int row(int number) {
        switch (number) {
            case 1:
                return 0;
            case 2:
                return 0;
            case 3:
                return 0;
            case 4:
                return 1;
            case 5:
                return 1;
            case 6:
                return 1;
            case 7:
                return 2;
            case 8:
                return 2;
            case 9:
                return 2;
            default:
                return 0;
        }
    }

    private static int column(int c) {
        switch (c) {
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 0;
            case 5:
                return 1;
            case 6:
                return 2;
            case 7:
                return 0;
            case 8:
                return 1;
            case 9:
                return 2;
            default:
                return 0;
        }
    }
}

