package project_euler;

    /* Exercise description:
   The sum of the squares of the first ten natural numbers is,
   12 + 22 + ... + 102 = 385
   The square of the sum of the first ten natural numbers is,
   (1 + 2 + ... + 10)2 = 552 = 3025
   Hence the difference between the sum of the squares of the first ten natural numbers
   and the square of the sum is 3025 − 385 = 2640.
   Find the difference between the sum of the squares of the first one hundred natural numbers
   and the square of the sum.
    Answer is: 25164150
    */

public class SumSquareDifference {
    public static void main(String[] args) {
        diff(sumOfSquares(100), squareOfTheSum(100));
    }

    private static int sumOfSquares(int number) {
        int sq;
        int sumSq = 0;
        for (int i = 1; i <= number; i++) {
            sq = i * i;
            sumSq = sumSq + sq;
        }
        return sumSq;
    }

    private static int squareOfTheSum(int number) {
        int sum = 0;
        for (int i = 1; i <= number; i++) {
            sum = sum + i;
        }
        sum = sum * sum;

        return sum;
    }

    private static void diff(int i, int j) {
        int diff;
        diff = j - i;
        System.out.println("The answer is " + diff);
    }
}

