package project_euler;

/*
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
Find the largest palindrome made from the product of two 3-digit numbers.
Answer is 906609
 */
public class LargestPalindromeProduct {
    public static void main(String[] args) {
        System.out.println("The answer is " + largestP());
    }

    private static int largestP() {
        int p;
        int largestPalindrome = 0;

        for (int i = 100; i < 1000; i++) {
            for (int j = 100; j < 1000; j++) {
                p = i * j;
                if (testPalindrome(p) && p > largestPalindrome) {
                    largestPalindrome = p;
                }
            }
        }
        return largestPalindrome;
    }

    private static boolean testPalindrome(int p) {

        String sNumber = String.valueOf(p);
        StringBuilder sbn = new StringBuilder(sNumber);
        sbn = sbn.reverse();
        String sNumber2 = sbn.toString();
        if (sNumber2.equals(sNumber)) {
            return true;
        } else return false;
    }

}

