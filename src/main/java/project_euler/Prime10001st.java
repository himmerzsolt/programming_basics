package project_euler;

import java.util.ArrayList;
/*
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
Answer: 104743

 */

public class Prime10001st {
    public static void main(String[] args) {
        prime();
    }

    private static void prime() {
        boolean isPrime;
        int i = 1;
        ArrayList<Integer> primes = new ArrayList<>();
        while (primes.size() < 10001) {
            i++;
            isPrime = true;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isPrime = false;
                }
            }
            if (isPrime) {
                primes.add(i);
            }
        }
        int s = primes.size();
        System.out.println("The answer is " + primes.get(s - 1));
    }
}
